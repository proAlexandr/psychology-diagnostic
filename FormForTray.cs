﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.VisualStyles;
using Microsoft.VisualBasic.ApplicationServices;

namespace rosskyru
{
	public partial class FormForTray : Form
	{
		private readonly TestForm testForm = new TestForm();
		private bool testModeOn = true;

		public FormForTray()
		{
			InitializeComponent();
			_notifyIcon.Visible = false;
		}


		public void HideMainWindow(object sender, EventArgs e)
		{
			Controls.Clear();

            // TODO: Correct test mode and timers
			var contextMenu = new ContextMenu(new[]
			{
//				new MenuItem("Выключить режим тестирования", OnTestClick),
				new MenuItem("Запустить", OnStudyClick),
                new MenuItem("Статистика", OnStatisticsClick),
//                new MenuItem("Авторы", Authors_Click),
				new MenuItem("Выход", OnExitClick)
			});

			_notifyIcon.Visible = true;
			_notifyIcon.ContextMenu = contextMenu;
			_notifyIcon.BalloonTipTitle = Text;
			_notifyIcon.BalloonTipText = "Для выбора опций, кликните правой кнопкой мыши по иконке";
			_notifyIcon.ShowBalloonTip(1000);

			WindowState = FormWindowState.Minimized;
			Hide();
		}

		private void CloseMainForm(object sender, EventArgs e)
		{
			Close();
		}

		private void OnStudyClick(object sender, EventArgs e)
		{
			testForm.Open();
		}

	    private void OnTestClick(object sender, EventArgs e)
	    {
            if (testModeOn)
            {
                testModeOn = false;
                _notifyIcon.ContextMenu.MenuItems[0].Text = "Включить режим тестирования";
                //_testForm.Hide();
                //_testForm = new TestForm();
                //_testForm.Controls.Clear();
                testForm.timer2.Stop();
                testForm.Hide();
            }
            else
            {
                testModeOn = true;
                _notifyIcon.ContextMenu.MenuItems[0].Text = "Выключить режим тестирования";
                //MessageBox.Show("I'm here");
                //  _testForm = new TestForm();
                testForm.InitTimer();
                //  _testForm.Show();
            }
            //this.Visible = false;
	    }

		public class Result
		{
			public DateTime Date;
			public double Score;
			public string Match;
		}

		private void OnStatisticsClick(object sender, EventArgs e)
		{
			if (!File.Exists(StatManagerEncryption.Path))
			{
				MessageBox.Show("Нет статистики!");
				return;
			}

			string[] stat;
			try
			{
				stat = File.ReadAllLines(StatManagerEncryption.Path).Select(StatManagerEncryption.Decrypt).ToArray();
			}
			catch (Exception)
			{
				MessageBox.Show("Файл со статистикой поврежден. Удалите его.");
				return;
			}
			if (stat.Length == 0)
			{
				MessageBox.Show("Нет статистики!");
				return;
			}

			var statForm = new Form();

			var results = new Result[stat.Length];

			for (int i = 0; i < stat.Length; i ++)
			{
				var parts = stat[i].Split('@');
				results[i] = new Result(){Date = DateTime.Parse(parts[0]), Score = Math.Min(1, Math.Round(double.Parse(parts[1]), 2)), Match = parts[2]};
			}

			statForm.Text = "Статистика состояния оператора";
			statForm.WindowState = FormWindowState.Maximized;
			statForm.Show();

			AddChart(statForm, results);
			AddTable(statForm, results);
			//MessageBox.Show(text);
		}

        private void OnDocumentationClick(object sender, EventArgs e)
        {
            var documentationForm = new Documentation();
            documentationForm.Show();
        }

		private void AddTable(Form statForm, Result[] results)
		{
			var grid = new DataGridView();

			results = results.OrderByDescending(r => r.Date).ToArray();
			grid.MaximumSize = new Size(500, statForm.Height);
			grid.Location = new Point(30, 30);
			grid.ReadOnly = true;
			grid.BackgroundColor = Color.White;

			grid.RowCount = results.Length;
			grid.ColumnCount = 4;
			grid.Columns[0].HeaderText = "Дата";
			grid.Columns[1].HeaderText = "Время";
			grid.Columns[2].HeaderText = "Результат";
			grid.Columns[3].HeaderText = "Совпадение";
			grid.Dock = DockStyle.Fill;
			grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

			for (int i = 0; i < results.Length; i++)
			{
				grid.Rows[i].Cells[0].Value = results[i].Date.ToShortDateString();
				grid.Rows[i].Cells[1].Value = results[i].Date.TimeOfDay;
				grid.Rows[i].Cells[2].Value = results[i].Score;
				grid.Rows[i].Cells[3].Value = results[i].Match + "%";
				if (results[i].Score < 0.3)
					grid.Rows[i].Cells[2].Style.BackColor = Color.Red;
				if (int.Parse(results[i].Match) < 50)
					grid.Rows[i].Cells[3].Style.BackColor = Color.Red;
			}

			statForm.Controls.Add(grid);
		}

		private static void AddChart(Form statForm, Result[] results)
		{
			var chart = new Chart();
			var chartArea = new ChartArea();

			chartArea.Name = "ChartArea1";
			chart.ChartAreas.Add(chartArea);
			chart.Size = new Size(statForm.Width - 500, statForm.Height);

			var stripLine1 = new StripLine {BackColor = Color.Red, StripWidth = 0.3, ForeColor = Color.Black};
			chart.ChartAreas[0].AxisY.StripLines.Add(stripLine1);

			// clear the chart
			chart.Series.Clear();

			// fill the chart
			var series = chart.Series.Add("My Series");
			chart.Series[0].BorderWidth = 5;
			chart.Series[0].BorderColor = Color.Black;

			series.ChartType = SeriesChartType.Line;
			series.XValueType = ChartValueType.Int32;
			for (int i = 0; i < results.Length; i++)
				series.Points.AddXY(results[i].Date, results[i].Score);


			// set view range to [0,max]
			chart.Series[0].XValueType = ChartValueType.DateTime;
			chart.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM-dd HH:mm";
			chart.ChartAreas[0].AxisX.Interval = 1;
			chart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Hours;
			chart.ChartAreas[0].AxisX.IntervalOffset = 1;

			chart.Series[0].XValueType = ChartValueType.DateTime;
			DateTime minDate = results.First().Date;
			DateTime maxDate = results.Last().Date;
			chart.ChartAreas[0].AxisX.Minimum = minDate.ToOADate();
			chart.ChartAreas[0].AxisX.Maximum = maxDate.ToOADate();

			// enable autoscroll
			chartArea.CursorX.AutoScroll = true;

			// let's zoom to [0,blockSize] (e.g. [0,100])
			chartArea.AxisX.ScaleView.Zoomable = true;
			chartArea.AxisX.ScaleView.Zoom(maxDate.ToOADate() - 0.1, maxDate.ToOADate());

			// disable zoom-reset button (only scrollbar's arrows are available)
			chartArea.AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;

			// set scrollbar small change to blockSize (e.g. 100)
			chartArea.AxisX.ScaleView.SmallScrollSize = 0.3;

			chart.Location = new Point(500, 0);
			statForm.Controls.Add(chart);
		}

		private void OnAuthorsClick(object sender, EventArgs e)
		{
			//TODO: Если вдруг код станет чистым, вернуть этот пункт в меню
			// Пишет Промах Александр (КН-301 2014-2015, 12 апреля 2015, 20:31)
			// Мы подобрали этот проект от нижеперечисленнх лиц, которые его писали приблизительно 2-3 года назад
			MessageBox.Show("Роман Горголь, КН-301" +
			                "\nМария Зырянова, КН-302" +
			                "\nКонстантин Ионин, КН-301", "Авторы"
				);
			// Пока код не будет чистым и аккуратным я не хочу возвражать данный пункт меню, 
			// ибо хвастаться этой программой пока нельзя.
			// Авторы второго поколения программы:
			//   Промах Александр - по сути тимлид :)    (КН-301)
			//   Алексей Кунгурцев - программист         (КН-301)
			//   Истомин Антон                           (КН-301)
			//   Владимир Руднев                         (КН-301)
			// 
			// Уважаемый читаталь, если ты вдруг работаешь в третьем поколении, то желаю тебе успехов :) 
			// Если возникнут нерешабельные проблемы со вторым поколением программы, то +79221261045                                                 
		}

		private void OnExitClick(object sender, EventArgs e)
		{
			Close();
        }

        private void OnTrainingClick(object sender, EventArgs e)
        {
            testForm.OpenForTraining();
        }
	}
}