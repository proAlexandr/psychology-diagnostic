﻿using System.Drawing;
using System.Windows.Forms;

namespace rosskyru
{
    public sealed class SequenceView : UserControl
    {
        private readonly int[] indices;
        private readonly Color[] colors = {Color.Red, Color.Blue, Color.Green};
        private const int D = 30;

        public SequenceView(int[] aIndices)
        {
            BackColor = Color.White;
            indices = aIndices;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var offsetX1 = 0;
            foreach (var index in indices)
            {
                var bulb = new Bulb(colors[index]);
                bulb.SwitchOn();

                bulb.SetBounds(offsetX1, 0, D, D);
                offsetX1 += 2*D;

                Controls.Add(bulb);
            }
        }
    }
}