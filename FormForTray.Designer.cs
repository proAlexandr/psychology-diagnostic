﻿namespace rosskyru
{
    partial class FormForTray
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormForTray));
            this._notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.labelInfo1 = new System.Windows.Forms.Label();
            this.labelInfo2 = new System.Windows.Forms.Label();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.labelInfo3 = new System.Windows.Forms.Label();
            this.labelInfo4 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStatistics = new System.Windows.Forms.Button();
            this.buttonDocumentation = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelInfo5 = new System.Windows.Forms.Label();
            this.labelInfo6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.labelInfo7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _notifyIcon
            // 
            this._notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("_notifyIcon.Icon")));
            this._notifyIcon.Text = "Диагностика состояния оператора";
            this._notifyIcon.Visible = true;
            // 
            // labelInfo1
            // 
            this.labelInfo1.AutoSize = true;
            this.labelInfo1.Location = new System.Drawing.Point(27, 18);
            this.labelInfo1.Name = "labelInfo1";
            this.labelInfo1.Size = new System.Drawing.Size(339, 13);
            this.labelInfo1.TabIndex = 0;
            this.labelInfo1.Text = "Вас приветствует программа диагностики состояния оператора!";
            // 
            // labelInfo2
            // 
            this.labelInfo2.AutoSize = true;
            this.labelInfo2.Location = new System.Drawing.Point(27, 42);
            this.labelInfo2.Name = "labelInfo2";
            this.labelInfo2.Size = new System.Drawing.Size(363, 13);
            this.labelInfo2.TabIndex = 1;
            this.labelInfo2.Text = "Если вы не хотите проходить тестирование, нажмите крестик сверху.";
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.Location = new System.Drawing.Point(333, 212);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(75, 23);
            this.buttonMinimize.TabIndex = 2;
            this.buttonMinimize.Text = "Свернуть";
            this.buttonMinimize.UseVisualStyleBackColor = true;
            this.buttonMinimize.Click += new System.EventHandler(this.HideMainWindow);
            // 
            // labelInfo3
            // 
            this.labelInfo3.AutoSize = true;
            this.labelInfo3.Location = new System.Drawing.Point(27, 66);
            this.labelInfo3.Name = "labelInfo3";
            this.labelInfo3.Size = new System.Drawing.Size(314, 13);
            this.labelInfo3.TabIndex = 3;
            this.labelInfo3.Text = "Если вы нажмёте \"Свернуть\", программа свернётся в трей.";
            // 
            // labelInfo4
            // 
            this.labelInfo4.AutoSize = true;
            this.labelInfo4.Location = new System.Drawing.Point(27, 90);
            this.labelInfo4.Name = "labelInfo4";
            this.labelInfo4.Size = new System.Drawing.Size(356, 13);
            this.labelInfo4.TabIndex = 5;
            this.labelInfo4.Text = "Кнопка \"Запустить\" запускает программу без сворачивания в трей.";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(185, 212);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(95, 23);
            this.buttonStart.TabIndex = 6;
            this.buttonStart.Text = "Запустить";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.OnStudyClick);
            // 
            // buttonStatistics
            // 
            this.buttonStatistics.Location = new System.Drawing.Point(30, 212);
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.Size = new System.Drawing.Size(96, 23);
            this.buttonStatistics.TabIndex = 7;
            this.buttonStatistics.Text = "Статистика";
            this.buttonStatistics.UseVisualStyleBackColor = true;
            this.buttonStatistics.Click += new System.EventHandler(this.OnStatisticsClick);
            // 
            // buttonDocumentation
            // 
            this.buttonDocumentation.Location = new System.Drawing.Point(30, 183);
            this.buttonDocumentation.Name = "buttonDocumentation";
            this.buttonDocumentation.Size = new System.Drawing.Size(96, 23);
            this.buttonDocumentation.TabIndex = 8;
            this.buttonDocumentation.Text = "Документация";
            this.buttonDocumentation.UseVisualStyleBackColor = true;
            this.buttonDocumentation.Click += new System.EventHandler(this.OnDocumentationClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 9;
            // 
            // labelInfo5
            // 
            this.labelInfo5.AutoSize = true;
            this.labelInfo5.Location = new System.Drawing.Point(27, 114);
            this.labelInfo5.Name = "labelInfo5";
            this.labelInfo5.Size = new System.Drawing.Size(334, 13);
            this.labelInfo5.TabIndex = 10;
            this.labelInfo5.Text = "Кнопка \"Статистика\" отображает все результаты прохождения.";
            // 
            // labelInfo6
            // 
            this.labelInfo6.AutoSize = true;
            this.labelInfo6.Location = new System.Drawing.Point(27, 136);
            this.labelInfo6.Name = "labelInfo6";
            this.labelInfo6.Size = new System.Drawing.Size(381, 13);
            this.labelInfo6.TabIndex = 11;
            this.labelInfo6.Text = "Кнопка \"Документация\" отображает описание программы и инструкции.";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(185, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Тренировка";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnTrainingClick);
            // 
            // labelInfo7
            // 
            this.labelInfo7.AutoSize = true;
            this.labelInfo7.Location = new System.Drawing.Point(27, 158);
            this.labelInfo7.Name = "labelInfo7";
            this.labelInfo7.Size = new System.Drawing.Size(334, 13);
            this.labelInfo7.TabIndex = 13;
            this.labelInfo7.Text = "Кнопка \"Тренировка\" запускает программу в пробном режиме.";
            // 
            // FormForTray
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 243);
            this.Controls.Add(this.labelInfo7);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelInfo6);
            this.Controls.Add(this.labelInfo5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDocumentation);
            this.Controls.Add(this.buttonStatistics);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelInfo4);
            this.Controls.Add(this.labelInfo3);
            this.Controls.Add(this.buttonMinimize);
            this.Controls.Add(this.labelInfo2);
            this.Controls.Add(this.labelInfo1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormForTray";
            this.Text = "Диагностика состояния оператора.";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private System.Windows.Forms.Label labelInfo1;
        private System.Windows.Forms.Label labelInfo2;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Label labelInfo3;
        private System.Windows.Forms.Label labelInfo4;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStatistics;
        private System.Windows.Forms.Button buttonDocumentation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelInfo5;
        private System.Windows.Forms.Label labelInfo6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelInfo7;
    }
}

