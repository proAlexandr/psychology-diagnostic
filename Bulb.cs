﻿using System.Drawing;
using System.Windows.Forms;

namespace rosskyru
{
    internal sealed class Bulb : UserControl
    {
        public readonly Color Color;
        public readonly Color BackgroundColor = Color.White;
        public bool On { get; private set; }

        private int diam;
        private int x;
        private int y;

        public Bulb(Color mColor)
        {
            BackColor = Color.White;
            Color = mColor;
            On = false;
        }

        public Bulb(Color mColor, Color backgroundColor)
        {
            BackColor = Color.White;
            Color = mColor;
            BackgroundColor = backgroundColor;
            On = false;
        }


        public void SetSize(int mX, int mY, int mDiam)
        {
            x = mX - diam/2;
            y = mY - diam/2;
            diam = mDiam;
            SetBounds(x, y, diam, diam);
        }

        public void SwitchOn()
        {
            On = true;
        }

        public void SwitchOff()
        {
            On = false;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
            var color = On ? Color : BackgroundColor;

            var circleRectangle = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1,
                ClientRectangle.Height - 1);

            pevent.Graphics.FillEllipse(new SolidBrush(color), circleRectangle);
            pevent.Graphics.DrawEllipse(new Pen(Color.Black), circleRectangle);
        }
    }
}