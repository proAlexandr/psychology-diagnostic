﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rosskyru
{
	public static class StatManagerEncryption
	{
		private static readonly string key = "wedfghjkdsfvvbsaiudfyksdaufhsdkaufhauhf";
		public static string Path = "stat";

		// Sets the read-only value of a file. 
		public static void SetFileReadAccess(bool SetReadOnly)
		{
			// Create a new FileInfo object.
			var fInfo = new FileInfo(Path);

			// Set the IsReadOnly property.
			fInfo.IsReadOnly = SetReadOnly;
		}

		public static string Encrypt(this string stringToEncrypt)
		{
			if (string.IsNullOrEmpty(stringToEncrypt))
			{
				throw new ArgumentException("An empty string value cannot be encrypted.");
			}

			if (string.IsNullOrEmpty(key))
			{
				throw new ArgumentException("Cannot encrypt using an empty key. Please supply an encryption key.");
			}

			System.Security.Cryptography.CspParameters cspp = new System.Security.Cryptography.CspParameters();
			cspp.KeyContainerName = key;

			System.Security.Cryptography.RSACryptoServiceProvider rsa = new System.Security.Cryptography.RSACryptoServiceProvider(cspp);
			rsa.PersistKeyInCsp = true;

			byte[] bytes = rsa.Encrypt(System.Text.UTF8Encoding.UTF8.GetBytes(stringToEncrypt), true);

			return BitConverter.ToString(bytes);
		}

		public static string Decrypt(this string stringToDecrypt)
		{
			string result = null;

			if (string.IsNullOrEmpty(stringToDecrypt))
			{
				throw new ArgumentException("An empty string value cannot be encrypted.");
			}

			if (string.IsNullOrEmpty(key))
			{
				throw new ArgumentException("Cannot decrypt using an empty key. Please supply a decryption key.");
			}

			try
			{
				System.Security.Cryptography.CspParameters cspp = new System.Security.Cryptography.CspParameters();
				cspp.KeyContainerName = key;

				System.Security.Cryptography.RSACryptoServiceProvider rsa = new System.Security.Cryptography.RSACryptoServiceProvider(cspp);
				rsa.PersistKeyInCsp = true;

				string[] decryptArray = stringToDecrypt.Split(new string[] { "-" }, StringSplitOptions.None);
				byte[] decryptByteArray = Array.ConvertAll<string, byte>(decryptArray, (s => Convert.ToByte(byte.Parse(s, System.Globalization.NumberStyles.HexNumber))));


				byte[] bytes = rsa.Decrypt(decryptByteArray, true);

				result = System.Text.UTF8Encoding.UTF8.GetString(bytes);

			}
			finally
			{
				// no need for further processing
			}

			return result;
		}
	}
}
