﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rosskyru
{
    public sealed class RadioBulb : UserControl
    {
        private Bulb[] bulbs;
        private const int D = 30;
        public int Value { get; private set; }

        public RadioBulb()
        {
            BackColor = Color.White;
            bulbs =
                new[] {Color.Red, Color.Blue, Color.Green}.Select(color => new Bulb(color, Color.FromArgb(20, color)))
                    .ToArray();

            for (var i = 0; i < 3; ++i)
            {
                var bulb = bulbs[i];
                var i1 = i;
                bulb.Click += (sender, args) =>
                {
                    foreach (var bulb1 in bulbs)
                    {
                        bulb1.SwitchOff();
                    }
                    bulb.SwitchOn();
                    Value = i1;
                    Refresh();
                };
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var offsetX1 = 0;
            foreach (var bulb in bulbs)
            {
                bulb.SetBounds(offsetX1, 0, D, D);
                Controls.Add(bulb);
                offsetX1 += 2*D;
            }
        }
    }
}