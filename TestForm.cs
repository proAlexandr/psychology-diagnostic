﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using LibUsbDotNet;
using LibUsbDotNet.Main;


namespace rosskyru
{
    public partial class TestForm : Form
    {
        private const int MinSec = 10;
        private const int MaxSec = 15;
        private static int n = 10;
        private readonly double[] L = new double[n];
        private readonly Bulb[] bulbs;
        private readonly Button closeButton = new Button {Text = "Закрыть"};
        private readonly Label error = new Label();
        private readonly ProgressBar progressBar = new ProgressBar();
        private readonly Button startButton = new Button {Text = "Начать"};
		private readonly Button nextButton = new Button { Text = "Далее", Font = new Font("courier", 12) };
        private readonly Timer timer = new Timer();
        private readonly Timer timerCountdown = new Timer();
        private double Q;
        private double a;
        private int count;
        private double ea;
        private bool lock1;
        private DateTime moment;
        private Sequence sequence;
        private Series series2;
        private TestFormStates state = TestFormStates.Start;
        private int secondsRemain;
        private Label secondsRemainLabel = new Label {BackColor = Color.White};
        public Timer timer2 = new Timer();
        private bool wasSound;
        private Timer soundTimer = new Timer();
        private SystemSound soundOnBulb;
        private RadioBulb[] radioBulbs;
        private int matchPercent;
        private bool TrainingMode;
		private Label labelTypeNext = new Label { BackColor = Color.White, Text = "Введите текущую последовательность", Font = new Font("courier", 20) };

        public TestForm()
        {
            InitializeBus();
            bulbs = new[]
            {
                new Bulb(Color.Red),
                new Bulb(Color.Blue),
                new Bulb(Color.Green)
            };

            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            FormClosing += OnFormClose;

            SetBounds(100, 100, 700, 250);
            MinimumSize = new Size(700, 250);
            timer.Tick += Unlock;
            timer2.Tick += OpenHandler;
            timerCountdown.Tick += TimerCountdownTick;
            soundTimer.Tick += PlaySound;
            soundTimer.Interval = 200;

            InitializeComponent();
            error.BackColor = Color.White;
            error.TextAlign = ContentAlignment.MiddleCenter;

            startButton.Click += OnStartButtonClick;
            closeButton.Click += CloseHandler;
            nextButton.Click += ShowResults;

            Font = new Font("courier", 15);
            Paint += OnTestFormPaint;
            KeyPreview = true;
        }

        public UsbDevice MyUsbDevice;

        private void InitializeBus()
        {
            UsbRegDeviceList allDevices = UsbDevice.AllDevices;
            foreach (UsbRegistry usbRegistry in allDevices)
            {
                if (!usbRegistry.Open(out MyUsbDevice)) continue;
                Console.WriteLine(MyUsbDevice.Info.ToString());
                return;
            }
        }

        private void PlaySound(object sender, EventArgs e)
        {
            var activeBulb = GetActiveBulb();
            if (activeBulb == null) return;

            var activeBulbSignal = "";
            var player = new SoundPlayer();

            if (activeBulb.Color == Color.Red)
            {
                player.SoundLocation = "beep-01a.wav";
                activeBulbSignal = "R"; //52h
            }
            if (activeBulb.Color == Color.Blue)
            {
                player.SoundLocation = "beep-02.wav";
                activeBulbSignal = "B"; //42h
            }
            if (activeBulb.Color == Color.Green)
            {
                player.SoundLocation = "beep-03.wav";
                activeBulbSignal = "G"; // 47h
            }

            if (MyUsbDevice != null)
            {
                var writer = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep01);
                int bytesWritten;
                writer.Write(Encoding.Default.GetBytes(activeBulbSignal), 2000, out bytesWritten);
            }
            player.Play();
        }

        private Bulb GetActiveBulb()
        {
            return bulbs.FirstOrDefault(bulb => bulb.On);
        }

        public void InitializeTest()
        {
            sequence = new Sequence();
            foreach (var bulb in bulbs)
            {
                bulb.SwitchOff();
            }
            bulbs[sequence.GetNext()].SwitchOn();
            wasSound = false;
            KeyDown += OnKeyDown;
            soundTimer.Start();
        }

        public void TimerCountdownTick(object sender, EventArgs e)
        {
            timerCountdown.Stop();
            if (secondsRemain < 3)
            {
                var sound = SystemSounds.Asterisk;
                sound.Play();
                secondsRemain++;
                secondsRemainLabel.Text = (3 - secondsRemain).ToString();
                timerCountdown.Interval = 1000;
                timerCountdown.Start();
            }
            else
            {
//                state = TestFormStates.EnteringSequence;
                state = TestFormStates.Testing;
                Controls.Clear();
                InitializeTest();
                moment = DateTime.Now;
                Refresh();
            }
        }

        public void OnStartButtonClick(object sender, EventArgs e)
        {
            state = TestFormStates.Countdown;

            Controls.Clear();

            secondsRemain = 0;
            secondsRemainLabel.SetBounds(Width/2 - 50, Height/2 - 50, 50, 50);
            Controls.Add(secondsRemainLabel);

            Font = new Font("courier", 36, FontStyle.Bold);
            secondsRemainLabel.Text = (3 - secondsRemain).ToString();
            timerCountdown.Interval = 1000;
            timerCountdown.Start();

            // костыль ***************
            var w = Width - 18;
            var h = Height - 39;
            var d = Math.Min(w/8, h/2);
            var offsetX = w/4;
            var offsetY = h/2;
            foreach (var bulb in bulbs)
            {
                bulb.SetSize(offsetX, offsetY, d);
                offsetX += 2*d;
            }
            // ***********
        }

        public void CloseHandler(object sender, EventArgs e)
        {
            Close();
            state = TestFormStates.Start;
            Visible = false;
            Controls.Clear();
            soundTimer.Stop();
        }

        public void InitTimer()
        {
            var rand = new Random();
            timer2.Interval = rand.Next(MinSec, MaxSec)*1000;
            timer2.Start();
        }

        public void OnTestFormPaint(object sender, PaintEventArgs e)
        {
            var graphics = CreateGraphics();
            graphics.Clear(Color.White);

            switch (state)
            {
                case TestFormStates.Start:
                    Font = new Font("courier", 15);
                    radioBulbs = null;
                    startButton.SetBounds(Width/2 - 100, Height/2 - 50, 200, 50);
                    startButton.Focus();
                    Controls.Add(startButton);
                    if (wasSound == false)
                    {
                        var sound = SystemSounds.Beep;
                        sound.Play();
                        wasSound = true;
                    }

                    break;
                case TestFormStates.Testing:
                    var w = Width - 18;
                    var h = Height - 39;

                    var d = Math.Min(w/8, h/2);
                    var offsetX = w/4;
                    var offsetY = h/2;

                    error.SetBounds(w/2 - 50, 20, 100, 40);
                    progressBar.SetBounds(w/2 - 50, h - 40, 100, 20);

                    foreach (var bulb in bulbs)
                    {
                        bulb.SetSize(offsetX, offsetY, d);
                        Controls.Add(bulb);
                        offsetX += 2*d;
                    }
                    if (TrainingMode)
                    {
                        var sequenceViewWigth1 = 30*(sequence.Length*2 - 1);
                        var realSequenceView1 = new SequenceView(sequence.Indices);
                        realSequenceView1.SetBounds(30, Height - 60, sequenceViewWigth1, 30);
                        Controls.Add(realSequenceView1);
                    }
                    Focus();
                    break;

                case TestFormStates.EnteringSequence:
                    var offsetY1 = 30;
                    if (radioBulbs == null)
                    {
                        radioBulbs = new RadioBulb[sequence.Length];
                        for (var i = 0; i < sequence.Length; ++i)
                        {
                            radioBulbs[i] = new RadioBulb();
                        }
                    }

					Font = new Font("courier", 12);
					labelTypeNext.SetBounds(Width / 2 - 300, offsetY1, 600, 50);
					labelTypeNext.TextAlign = ContentAlignment.MiddleCenter;
					Controls.Add(labelTypeNext);
		            offsetY1 += 80;

					foreach (var radioBulb in radioBulbs)
                    {
                        radioBulb.SetBounds(Width / 2 - 30*5 / 2, offsetY1, 30*5, 30);
                        Controls.Add(radioBulb);
                        offsetY1 += 60;
                    }
                    Font = new Font("courier", 12);
                    nextButton.SetBounds(Width / 2 - nextButton.Width / 2, offsetY1, 100, 30);
                    Controls.Add(nextButton);
                    
					
					
					nextButton.Focus();

                    break;

                case TestFormStates.Result:
                    Font = new Font("courier", 12);

                    var chart1 = BuildResultsChart();
                    var labelQ = new Label
                    {
                        BackColor = Color.White,
                        Text = "Q = " + Q + "\nОценка состояния оператора:\n" + AssessmentToString()
                    };
                    labelQ.SetBounds(800, 100, 300, 200);
                    var sequenceMatchLabel = new Label
                    {
                        BackColor = Color.White,
                        Text = "Последовательность распознана правильно на " + matchPercent + "%"
                    };
                    sequenceMatchLabel.SetBounds(30, 650, 600, 50);
                    closeButton.SetBounds((Width - 75)/2, 700, 150, 50);

                    var sequenceViewWigth = 30*(sequence.Length*2 - 1);
                    var realSequenceView = new SequenceView(sequence.Indices);
                    var enteredSequenceView = new SequenceView(GetRadioBulbsValue());

					var l1 = new Label
					{
						BackColor = Color.White,
						Text = "Текущая последовательность:"
					};
					l1.SetBounds(30, 450, 300, 20);
                    realSequenceView.SetBounds(30, 480, sequenceViewWigth, 30);
					var l2 = new Label
					{
						BackColor = Color.White,
						Text = "Вы ввели:"
					};
					l2.SetBounds(30, 550, 300, 20);
                    enteredSequenceView.SetBounds(30, 580, sequenceViewWigth, 30);

					Controls.Add(l1);
					Controls.Add(l2);
                    Controls.Add(chart1);
                    Controls.Add(labelQ);
                    Controls.Add(closeButton);
                    Controls.Add(realSequenceView);
                    Controls.Add(enteredSequenceView);
                    Controls.Add(sequenceMatchLabel);

                    break;
            }
        }

        private int[] GetRadioBulbsValue()
        {
            return radioBulbs.Select(x => x.Value).ToArray();
        }

        private Chart BuildResultsChart()
        {
            var chartArea2 = new ChartArea();
            var legend2 = new Legend();
            series2 = new Series();
            var chart1 = new Chart();
            ((ISupportInitialize) (chart1)).BeginInit();
            SuspendLayout();

            chartArea2.Name = "ChartArea1";
            chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            // this.chart1.Legends.Add(legend2);
            chart1.Location = new Point(12, 12);
            chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            // series2.Legend = "Legend1";
            series2.Name = "Series1";
            chart1.Series.Add(series2);
            chart1.Size = new Size(485, 358);
            chart1.TabIndex = 0;
            chart1.Text = "Диаграмма";

            ((ISupportInitialize) (chart1)).EndInit();
            chart1.Size = new Size(700, 300);
            chart1.Palette = ChartColorPalette.SeaGreen;

            chart1.Titles.Add("Результаты теста");
            for (int i = 0; i < 10; i++)
            {
                L[i] = L[i];
            }
            chart1.Series["Series1"].Points.DataBindXY(new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, L);
            chart1.Series["Series1"].ChartType = SeriesChartType.Line;
            chart1.Series["Series1"].IsValueShownAsLabel = false;
            chart1.ChartAreas[0].AxisX.IsMarginVisible = false;
            chart1.Series["Series1"].IsValueShownAsLabel = true;
            return chart1;
        }

        private void ShowResults(object sender, EventArgs e)
        {
            if (!TrainingMode)
            {
				CheckSequenceMatch();				
                WriteStat();
            }
            state = TestFormStates.Result;
            CheckSequenceMatch();

            Controls.Clear();
            Refresh();
        }

        private void CheckSequenceMatch()
        {
            var trueIndices = sequence.Indices;
            var enteringIndices = GetRadioBulbsValue();
            matchPercent = CalcSequenceMatch(trueIndices, enteringIndices);
        }

        public int[] ShiftArray(int[] arr, int shift)
        {
            int[] newInts = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                newInts[i] = arr[(((i + shift)%arr.Length)
                                  + arr.Length)%arr.Length];
            return newInts;
        }

        private int CalcSequenceMatch(int[] trueIndices, int[] enteringIndices)
        {
            int mn =
                Enumerable.Range(0, trueIndices.Length)
                    .Select(i => ShiftArray(trueIndices, i))
                    .Select(p => ComputeLevenshtein(p, enteringIndices))
                    .Min();

            int res = (trueIndices.Length - mn)*100/trueIndices.Length;
            return res;
        }

	    private int ComputeDist(int[] s, int[] t)
	    {
		    int d = 0;
		    for (int i = 0; i < s.Length; i++)
			    if (s[i] != t[i])
				    d++;
		    return d;
	    }

        private int ComputeLevenshtein(int[] s, int[] t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        private string AssessmentToString()
        {
            if (Q < 0.3) return "неудовлетворительно";
            if (Q >= 0.3 && Q < 0.5) return "удовлетворительно";
            if (Q >= 0.5 && Q < 0.75) return "хорошо";
            return "отлично";
        }

        private void WriteStat()
        {
            if (!File.Exists(StatManagerEncryption.Path))
            {
                var myFile = File.Create(StatManagerEncryption.Path);
                myFile.Close();
            }


            var lines = new List<string>()
            {
                DateTime.Now.ToString() + '@' + Q + '@' + matchPercent
            };

            var encoded = lines.Select(l => StatManagerEncryption.Encrypt(l));
            StatManagerEncryption.SetFileReadAccess(false);
            File.AppendAllLines(StatManagerEncryption.Path, encoded);
            StatManagerEncryption.SetFileReadAccess(true);
        }

        private double F(double x)
        {
            double res = 0;
            for (var i = 0; i < 10; i++) res += Math.Exp(-i*x);
            return res;
        }

        private double FindParamA(double x)
        {
            if (x <= 1) return 50;
            double l = -50, r = 50;

            while (Math.Abs(l - r) > 1e-9)
            {
                double m = (l + r)/2;
                double fl = F(l) - x;
                double fm = F(m) - x;

                if (fl*fm > 0) l = m;
                else r = m;
            }

            return (l + r)/2;
        }

        protected override bool IsInputKey(Keys keyData)
        {
            return true;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            Bulb bulb = null;

            switch (e.KeyCode)
            {
                case Keys.Left:
                    bulb = bulbs[0];
                    break;
                case Keys.Down:
                    bulb = bulbs[1];
                    break;
                case Keys.Right:
                    bulb = bulbs[2];
                    break;
                case Keys.Escape:
                    CloseHandler(null, null);
                    break;
                default:
                    return;
            }

            if (bulb != null && bulb.On)
            {
                if (lock1)
                    return;

                bulb.SwitchOff();
                var next = sequence.GetNext();
                progressBar.Value++;

                if (next >= 0)
                {
                    bulbs[next].SwitchOn();
                }
                else
                {
                    progressBar.Value = 0;
                    //закончили
                    //засечь время
                    DateTime current = DateTime.Now;
                    L[count++] = (current.Ticks - moment.Ticks)/10000;

                    moment = current;
                    if (count == n)
                    {
                        //показать результат
                        for (var i = 0; i < n; i++)
                            L[i] = (int) L[i]/sequence.Length;

                        var L1 = L.Take(3).Max();
                        var L8 = L.Skip(n - 5).Min();

                        ea = (L.Sum() - n*L8)/(L1 - L8);
                        a = FindParamA(ea);

                        var k = 0.544 + 0.251*0.001*L1 + 0.520*0.001*L8 - 0.514*a;
                        Q = Math.Max(1 - k, 0);
	                    Q = Math.Min(1, Q);

                        state = TestFormStates.EnteringSequence;
                        soundTimer.Stop();
                        KeyDown -= OnKeyDown;
                        Controls.Clear();
                        count = 0;
                    }
                    else
                    {
                        next = sequence.GetNext();
                        bulbs[next].SwitchOn();
                    }
                }
            }
            else
            {
                // лочим при неправильном нажатии на 100 мс
                lock1 = true;
                timer.Interval = 100;
                timer.Start();
                error.Text = "Error";
            }
            Controls.Clear();
        }

        private void Unlock(object sender, EventArgs e)
        {
            timer.Stop();
            lock1 = false;
            error.Text = "";
        }

        private void OpenHandler(object sender, EventArgs e)
        {
            timer2.Stop();
            Open();
        }

        public void Open()
        {
            Controls.Clear();
            Refresh();
            Visible = true;
            TrainingMode = false;
        }

        public void OpenForTraining()
        {
            Open();
            TrainingMode = true;
        }


        public void OnFormClose(object sender, FormClosingEventArgs e)
        {
            //UsbDevice.Exit();
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        private enum TestFormStates
        {
            Start,
            Testing,
            Result,
            Countdown,
            EnteringSequence
        }
    }
}