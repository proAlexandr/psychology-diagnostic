﻿using System;
using System.Collections.Generic;

namespace rosskyru
{
    internal class Sequence
    {
        private static readonly int[] From = {-1, 0, 1, 0, 2, 1, 2};
        private static readonly int[] To = {-1, 1, 2, 2, 1, 0, 0};

        private static readonly List<int[]> Sequences = new List<int[]>
        {
			//new [] {1},
			new[] {1, 2, 4, 5},
			new[] {1, 5, 3, 6},
			new[] {3, 4, 2, 6},
			new[] {1, 2, 6, 1, 5},
			new[] {2, 4, 5, 3, 4},
			new[] {3, 4, 5, 3, 6},
			new[] {1, 2, 4, 5, 3, 6},
			new[] {1, 5, 3, 4, 2, 6},
			new[] {1, 2, 4, 5, 1, 2, 6},
			new[] {1, 2, 6, 3, 4, 2, 6},
			new[] {1, 2, 6, 1, 5, 3, 4, 5},
			new[] {1, 2, 4, 5, 3, 4, 2, 6},
			new[] {1, 2, 6, 3, 4, 5, 3, 6},
			new[] {1, 2, 4, 5, 3, 6, 1, 5},
			new[] {1, 2, 6, 3, 4, 2, 4, 5},
			new[] {1, 5, 3, 6, 3, 4, 2, 6},
			new[] {1, 2, 4, 5, 1, 5},
			new[] {1, 5, 1, 5, 3, 6},
			new[] {1, 2, 4, 2, 6, 1, 5},
			new[] {1, 5, 3, 4, 2, 4, 5},
			new[] {2, 4, 5, 3, 6, 3, 4}
        };

        public readonly int[] Indices;
        private int count;
        private int index;

        public int Length
        {
            get { return Indices.Length; }
        }

        public Sequence()
        {
            var sequence = Sequences[new Random().Next(Sequences.Count - 1)];

            Indices = new int[sequence.Length];
            Indices[0] = From[sequence[0]];
            for (var i = 0; i < sequence.Length - 1; i++)
            {
                Indices[i + 1] = To[sequence[i]];
            }
            index = 0;
        }

        public int GetNext()
        {
            if (count == Length + 1)
            {
                count = 0;
                return -1;
            }
            count++;
            return Indices[(index++)%(Indices.Length)];
        }
    }
}